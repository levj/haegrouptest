<?php
class Database {
    private static $instance = null;
    private $dbh;

    private function __construct() {
        $this->dbh = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASS);
    }

    public static function getInstance() {
        if(self::$instance == null) {
            self::$instance = new Database();
        }

        return self::$instance;
    }

    public function getConnection()
    {
        return $this->dbh;
    }
}