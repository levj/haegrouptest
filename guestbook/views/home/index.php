<div class="main-container">
    <div class="side-bar">
        <div class="msg-head">
            <a href="#">
                <img src="assets/images/logo-hae-group.png" />
            </a>
            <p class="side-title">Guestbook</p>
            <p class="side-description">Feel free to leave us a short message to tell us what you think to our services</p>
            
            <button class="btn-post-msg" data-toggle="modal" data-target="#postModal">Post a message</button>
            <!-- Modal -->
            <div class="modal fade" id="postModal" tabindex="-1" role="dialog" aria-labelledby="postModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="postModalLabel">Post your message</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="guest_name" class="col-form-label">Your name:</label>
                                    <input type="text" class="form-control" id="guest_name">
                                </div>
                                <div class="form-group">
                                    <label for="msg_body" class="col-form-label">Message:</label>
                                    <textarea class="form-control" id="msg_body"></textarea>
                                </div>
                            </form>
                            <div class="modal-alert alert" role="alert"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="button" id="add_msg" name="submit" value="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php 
            if($this->isAdmin)
            {
                echo '<a class="btn-adm" href="users/logout">Logout</a>';                
            }
            else {
                echo '<a class="btn-adm" href="users/login">Admin Login</a>';
            }        
        ?>
    </div>

    <div class="msg-container">
        <div id="msg_list" class="msg-list">                                        
            <!-- data will be inserted here -->
        </div>
        
             
        
        <!-- Edit Modal -->
        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="editModalLabel">Edit message</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="edit_guest_name" class="col-form-label">Name:</label>
                                <input type="text" class="form-control" id="edit_guest_name">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Message:</label>
                                <textarea class="form-control" id="edit_msg_body"></textarea>
                            </div>                            
                        </form>
                        <div class="modal-alert alert" role="alert"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" id="edit_msg" name="submit" value="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Delete Modal -->
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLabel">Delete message</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Do you want to delete this message?</p>
                        <div class="modal-alert alert" role="alert"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" id="delete_msg" name="submit" value="submit" class="btn btn-primary">Yes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>