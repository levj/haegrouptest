<?php
class Messages extends Controller {

    public function add() {
        $this->assetter->addJs('/assets/js/message_add.js');
        
        $viewModel = new MessageModel();
        $this->returnView($viewModel->index(), true);
    }

    public function read() {
        $viewModel = new MessageModel();        
        echo json_encode($viewModel->getAll());
    }

    public function create() {
        $viewModel = new MessageModel();
        echo json_encode($viewModel->create());
    }

    public function update() {
        if(!isset($_SESSION['is_logged_in']) || $_SESSION['is_logged_in'] != true || !isset($_SESSION['is_admin']) || $_SESSION['is_admin'] != true) {
            echo json_encode(array('status' => 'error', 'error' => 'You dont have permission to do this!'));
            exit;
        }        
        $viewModel = new MessageModel();        
        echo json_encode($viewModel->update());
    }

    public function delete() {
        if(!isset($_SESSION['is_logged_in']) || $_SESSION['is_logged_in'] != true || !isset($_SESSION['is_admin']) || $_SESSION['is_admin'] != true) {
            echo json_encode(array('status' => 'error', 'error' => 'You dont have permission to do this!'));
            exit;
        }  
        $viewModel = new MessageModel();        
        echo json_encode($viewModel->delete()); 
    }
}