<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GUEST BOOK</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/assets/css/font-awesome-all.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css" />    
    <?php $this->assetter->outputCss() ?>
</head>
<body>    

    
        
    <?php require($view); ?>
    
    
    <script src="/assets/js/jquery-3.3.1.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        <?php echo 'var ROOT_URL = "' . ROOT_URL . '";'; 
            echo 'var MSG_DISPLAY_NUM = "' . MSG_DISPLAY_NUM . '";';
            echo 'var ADM = ' . ($this->isAdmin ? 'true' : 'false') . ';';
        ?>
    </script>    
    <script src="/assets/js/main.js"></script>    
    <?php $this->assetter->outputJs() ?>    
</body>
</html>