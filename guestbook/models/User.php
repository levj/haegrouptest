<?php
class UserModel extends Model {
    public function register() {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);        
        var_dump($post);
        if($post['submit']) {
            if(empty($post['name']) || empty($post['email']) || empty($post['password'])) {
                Notification::setNotification('Please fill in all fields!', 'error');
                return;
            }
            $pwd = password_hash($post['password'], PASSWORD_DEFAULT);
            $this->query('INSERT INTO users (name, email, password) VALUES (:name, :email, :password)');
            $this->bind(':name', $post['name']);
            $this->bind(':email', $post['email']);
            $this->bind(':password', $pwd);
            $this->execute();
            if($this->lastInsertId()) {
                header('Location: ' . ROOT_URL . 'users/login');
            }
        }
        return;
    }

    public function login() {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);      

        if($post['submit']) {          
            
            $this->query('SELECT * FROM users WHERE email = :email');
            $this->bind(':email', $post['email']);            
            $row = $this->getOne();

            if($row && password_verify($post['password'], $row['password'])) {
                return $row;
            }
        }
        return false;
    }
}