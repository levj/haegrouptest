<?php

// Define DB Params
define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASS", "");
define("DB_NAME", "guest_book");

// Define URL
define("ROOT_PATH", "/");
define("ROOT_URL", "http://guestbook/");

// const
define("MSG_DISPLAY_NUM", 6);
define("DEBUG", false);