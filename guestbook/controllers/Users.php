<?php
class Users extends Controller {
    // public function register() {
    //     $viewModel = new UserModel();
    //     $this->returnView($viewModel->register(), true);
    // }

    public function login() {
        // if already loggedin
        if(isset($_SESSION['is_logged_in']) && $_SESSION['is_logged_in'] == true) {
            header('Location: ' . ROOT_URL);
            exit;
        }

        $viewModel = new UserModel();
        $userData = $viewModel->login();
        if($userData) {
            $_SESSION['is_logged_in'] = true;
            $_SESSION['user_data'] = array(
                'id'    => $userData['id'],
                'name'  => $userData['name'],
                'email' => $userData['email'],
                'access_type' => $userData['access_type']
            );
            $_SESSION['is_admin'] = intval($userData['access_type']) ===  1;

            header('Location: ' . ROOT_URL);
            exit;
        }
        $this->returnView(array(), true);
    }

    public function logout() {
        unset($_SESSION['is_logged_in']);
        unset($_SESSION['user_data']);
        unset($_SESSION['is_admin']);
        session_destroy();
        header('Location: ' . ROOT_URL);
    }
}