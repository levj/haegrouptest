$(document).ready(function() {
    var msg_data;
    var msg_ind = 0;
    var item_ind = 0;
    var page_ind = 1;
    var max_page_display = 3;


    // 
    $("#add_msg").on("click", function(event) {        
        var guestName = $("#guest_name").val();
        var body = $("#msg_body").val();        

        if(!guestName || !guestName.trim() || !body || !body.trim()) {
            showAlert("#postModal", "error", "Please don't leave fields empty!");
        }
        else if(guestName.length > 50) {
            showAlert("#postModal", "error", "Your name is too long!");
        }
        else if(body.length > 350) {
            showAlert("#postModal", "error", "Your message is too long!");
        }
        else {
            var url = ROOT_URL + "messages/create";            
            $.post(url, {
                guest_name: guestName,
                body: body            
            }).done(function (data) {
                var res;
                
                try {
                    res = JSON.parse(data);
                
                    if(res && res.status == "success") {
                        showAlert("#postModal", "success", "Message added successfully!");
                        setTimeout(() => {
                            $('#postModal').modal('hide');
                            updateMessageList();
                        }, 1000);
                    } else {
                        showAlert("#postModal", "error", "Add message failed!" + res.error);
                    }                
                }
                catch (err) {
                    showAlert("#postModal", "error", "Add message failed!");
                }

            }).fail(function () {                
                showAlert("#postModal", "error", "Cannot add message!");

            }).always(function () {
                
            });

            event.preventDefault();    
        }
    });

    // add modal show, clear prev data
    $('#postModal').on('show.bs.modal', function (e) {             
        $('#guest_name').val('');
        $('#msg_body').val('');
    });

    // edit message
    $('#editModal').on('show.bs.modal', function (e) {                 
        if(msg_data[item_ind]) {
            var edit_guest_name = $('#edit_guest_name');
            var edit_msg_body = $('#edit_msg_body');
            
            edit_guest_name.val(msg_data[item_ind].guest_name);
            edit_guest_name.data('origin', msg_data[item_ind].guest_name);
            edit_msg_body.val(msg_data[item_ind].body);
            edit_msg_body.data('origin', msg_data[item_ind].body);
        }
    });


    // 
    $("#edit_msg").on("click", function(event) {        
        var guestName = $("#edit_guest_name").val();
        var body = $("#edit_msg_body").val();   
        var prevName = $("#edit_guest_name").data('origin');
        var prevBody = $("#edit_msg_body").data('origin');     

        if(guestName === prevName && body === prevBody) {
            showAlert("#editModal", "error", "You changed nothing!");  
        }
        else if(!guestName || !guestName.trim() || !body || !body.trim()) {
            showAlert("#editModal", "error", "Please don't leave fields empty!");            
        }
        else if(guestName.length > 50) {
            showAlert("#postModal", "error", "Your name is too long!");
        }
        else if(body.length > 350) {
            showAlert("#postModal", "error", "Your message is too long!");
        }
        else {            
            var id = msg_data[item_ind].id;
            var url = ROOT_URL + "messages/update";
            $.post(url, {
                guest_name: guestName,
                body: body,
                id: id            
            }).done(function (data) {
                var res;
                
                try {                    
                    res = JSON.parse(data);
                
                    if(res && res.status == "success") {
                        showAlert("#editModal", "success", "Message edited successfully!");
                        setTimeout(() => {
                            $('#editModal').modal('hide');
                            updateMessageList();
                        }, 1000);
                    } else {
                        showAlert("#editModal", "error", "Edit message failed!" + res.error);
                    }                
                }
                catch (err) {
                    showAlert("#editModal", "error", "Edit message failed!" + err);
                }

            }).fail(function () {                
                showAlert("#editModal", "error", "Cannot edit message!");

            }).always(function () {
                
            });

            event.preventDefault();
        }
    });

    // 
    $("#delete_msg").on("click", function(event) {                                
        var id = msg_data[item_ind].id;
        var url = ROOT_URL + "messages/delete";
        $.post(url, {
            id: id            
        }).done(function (data) {
            var res;
            
            try {                
                res = JSON.parse(data);
            
                if(res && res.status == "success") {
                    showAlert("#deleteModal", "success", "Message deleted successfully!");
                    setTimeout(() => {
                        $('#deleteModal').modal('hide');
                        updateMessageList();
                    }, 1000);
                } else {
                    showAlert("#deleteModal", "error", "Delete message failed!" + res.error);
                }                
            }
            catch (err) {
                showAlert("#deleteModal", "error", "Delete message failed!" + err);
            }

        }).fail(function () {                
            showAlert("#deleteModal", "error", "Cannot delete message!");

        }).always(function () {
            
        });

        event.preventDefault();
        
    });

    $('.modal').on('hide.bs.modal', function (e) {     
        $('.modal-body .modal-alert').removeClass('active');
    });

    function registerClickEvents(){
        $('.msg-options .msg-option').each(function(ind) {            
            $(this).on('click', function(event){
                item_ind = $(this).data('ind');                
            });
        });        
    }
    
    function updateMessageList() {        
        var url = ROOT_URL + "messages/read"; 

        $.get(url).done(function (data) {                
            try {
                msg_data = JSON.parse(data);                
                if(msg_data && msg_data.length > 0) {                    
                    showMessages(true);    
                    showPagination();                    
                } else {
                    var msg_list = $("#msg_list");
                    msg_list.empty();
                    $(".msg-pagination").remove();
                    msg_list.append("<h3 style='color: #fff;'>There's no message yet, post your message!</h3>");
                }                
            }
            catch (err) {
                console.log(err);
            }

        }).fail(function () {                            

        }).always(function () {
            
        });
    }

    function showMessages(refresh = false) {        
        if(refresh == true) {
            page_ind = 1;
        }
        var msg_list = $("#msg_list");
        msg_list.empty();
        var n;   
        var remain = (msg_data.length - ((page_ind - 1) * MSG_DISPLAY_NUM));
        if(remain > MSG_DISPLAY_NUM) {            
            n = MSG_DISPLAY_NUM;
        } else {
            n = remain;
        }
        for(i = 0; i < n; i++) {            
            var idx = i + (page_ind - 1) * MSG_DISPLAY_NUM;
            msg_list.append(getItem(idx));
        }       

        registerClickEvents();
    }

    function getItem(id) {
        if(!msg_data || !msg_data[id])
        {
            return '';
        }        
        var date = new Date(msg_data[id].last_update);  
        var adm_options = '<ul class="msg-options">'
                            +    '<li>'
                            +        '<a class="msg-option" href="messages/edit" data-ind="' + id + '" data-toggle="modal" data-target="#editModal"><i class="fas fa-pencil-alt"></i></a>'
                            +    '</li>'
                            +    '<li>'
                            +        '<a class="msg-option" href="messages/delete" data-ind="' + id + '" data-toggle="modal" data-target="#deleteModal"><i class="fas fa-trash"></i></a>'
                            +    '</li>'
                            +'</ul>';

        var item = '<div class="msg-item">'
                        + '<p class="msg-content">' + msg_data[id].body + '</p>'
                        + '<div class= msg-footer>'
                        +    '<div class="msg-info">'
                        +        '<p class="msg-author">' + msg_data[id].guest_name + '</p>'
                        +        '<p class="msg-date">' + formatDate(date) + '</p>'                        
                        +    '</div>'
                        +    (ADM == true ? adm_options : '')
                        + '</div>'
                +  '</div>';

        return item;
    }

    function showAlert(id, type, msg) {       
        var mod = $(id + ' .modal-body .modal-alert');
        mod.addClass('active');
        mod.text(msg);

        if(type == 'success') {
            mod.removeClass("alert-danger");
            mod.addClass("alert-success");
        } else if (type == 'error'){
            mod.removeClass("alert-success");
            mod.addClass("alert-danger");
        }
    }

    function formatDate(date) {
        var short_monthes = [
          "Jan", "Feb", "Mar",
          "Apr", "May", "Jun", "Jul",
          "Aug", "Sep", "Oct",
          "Nov", "Dec"
        ];
      
        var day = date.getDate();
        var m = date.getMonth();
        var year = date.getFullYear();

        var time = date.toLocaleTimeString('en-US');
      
        return day + ' ' + short_monthes[m] + ', ' + year + ' at ' + time;
    }

    function showPagination() {        
        var msg_container = $(".msg-container");
        var total_pages = Math.ceil(msg_data.length / MSG_DISPLAY_NUM);        
        var pag = '<ul class="msg-pagination">';
        var li = '';
        var remain;
        if(page_ind % max_page_display == 0) {
            remain = total_pages - page_ind + max_page_display;
        } else {
            remain = total_pages - page_ind + 1;
        }
        var max = remain > max_page_display ? max_page_display : remain;
        
        $(".msg-pagination").remove();
        
        li += '<li class="pag-item">\
                    <a class="pag-prev" href="#" aria-label="Previous">\
                            <i class="fas fa-angle-left"></i>\
                    </a>\
                </li>';
        
        for(i = 0; i < max; i++) {
            var id = i + page_ind;
            if(page_ind % max_page_display == 0) {
                id = page_ind - max_page_display + 1 + i;
            }
            li += '<li class="pag-item' + (i == 0 ? ' active' : '') + '"><a class="pag-link" href="#" data-ind="' + id + '">' + id + '</a></li>';
        }
        
        li += '<li class="pag-item">\
                    <a class="pag-next" href="#" aria-label="Previous">\
                            <i class="fas fa-angle-right"></i>\
                    </a>\
                </li>';
        
        pag += li;
        pag += '</ul>';

        msg_container.append($(pag));

        updatePagination();

        //register pagination click events
        $('.msg-pagination .pag-item .pag-link').each(function(ind) {            
            $(this).on('click', function(event){                
                page_ind = $(this).data('ind');
                showMessages();
                updatePagination();
            });
        });
        $('.pag-prev').on('click', function(event){            
            page_ind -= 1;
            showMessages();
            if(page_ind % max_page_display == 0)
            {
                showPagination(); 
            } else {
                updatePagination();
            }
        });
        $('.pag-next').on('click', function(event){            
            page_ind += 1;
            showMessages();
            if(page_ind % max_page_display == 1)
            {
                showPagination(); 
            } else {
                updatePagination();
            }
        });        
    }
    function updatePagination() {
        var total_pages = Math.ceil(msg_data.length / MSG_DISPLAY_NUM);
        if(page_ind == 1) {
            $('.pag-prev').addClass('disabled');
        } else {
            $('.pag-prev').removeClass('disabled');
        }
        if(page_ind == total_pages) {
            $('.pag-next').addClass('disabled');
        } else {
            $('.pag-next').removeClass('disabled');
        }

        $('.pag-item').removeClass('active');
        $('.pag-link[data-ind="' + page_ind + '"]').closest('li').addClass('active');
    }

    updateMessageList();    
});