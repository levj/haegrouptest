# haegrouptest

Thank you for visit my project.

# DESCRIPTION
- Create a guestbook to let visitors of a website submit messages. The messages must be visible on the
- site to everyone.
- The site�s admin user should be able to delete and edit the messages.
- The message should be submitted using AJAX rather than standard form submission.
- The messages should be stored in an SQL database.
- The HTML output should be valid HTML5 and responsive for different screen sizes.

# REQUIREMENTS
- Language: PHP 7
- Web server: Apache
- SQL database server: MySQL
- Operating system: Cross platform
- Third Party PHP Frameworks: Not Allowed.
- Third Party JavaScript and CSS libraries: Allowed, e.g. JQuery, Bootstrap

# SQL schema
- You can find it in guest_book.sql inside the guestbook project folder
- I added a default admin user for this project:
- User name: admin@guestbook.com
- Password: 123456

# I know this project still have many things to do. Anyway, I'm very looking forward to your review & feedback.

Please let me know if you have any question or have any issue with the project.

# Thank you.