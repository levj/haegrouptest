<?php

session_start();

// Include config
require('config.php');
require('classes/Bootstrap.php');
require('classes/Controller.php');
require('classes/Model.php');
require('classes/Notification.php');

require('controllers/Home.php');
require('controllers/Messages.php');
require('controllers/Users.php');

require('models/Home.php');
require('models/Message.php');
require('models/User.php');

if(!DEBUG) {
    error_reporting(0);
}

$bootstrap = new Bootstrap($_GET);
$controller = $bootstrap->createController();
if($controller) {
    $controller->executeAction();
}