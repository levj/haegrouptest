<?php
include_once "Assetter.php";

abstract class Controller {
    protected $request;
    protected $action;
    protected $assetter;

    public function __construct($action, $request) {
        $this->action = $action;
        $this->request = $request;    
        $this->assetter = new Assetter();
    }

    public function executeAction() {
        return $this->{$this->action}();
    }

    public function returnView($data = array(), $fullview = false) {
        $view = 'views/' . get_class($this) . '/' . $this->action . '.php';
        if($fullview) {
            require('views/main.php');
        } else {
            require($view);
        }
    }
}