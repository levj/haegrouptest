<div class="card">  
    <div class="card-header">
        Post your message!
    </div>
    <div class="card-body">
        <form method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
            <div class="form-group">
                <label for="title">Guest name</label>
                <input type="text" class="form-control" id="guest_name" name="guest_name">                
            </div>
            <div class="form-group">
                <label for="body">Body</label>
                <textarea class="form-control" id="msg_body" name="msg_body"></textarea>
            </div>

            <button type="submit" id="add_msg" name="add_msg" class="btn btn-primary" value="submit">Submit</button>
            <a class="btn btn-danger" href="<?php echo ROOT_PATH; ?>messages">Cancel</a>
        </form>
    </div>
</div>