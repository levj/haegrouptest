<form class="form-signin text-center" method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
    <img class="login-logo" src="/assets/images/logo-hae-group.png" alt="">
    <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
    <label for="email" class="sr-only">Email address</label>
        <input type="email" id="email" name="email" class="form-control" placeholder="Email address" required autofocus>
    <label for="password" class="sr-only">Password</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>    
    <button class="btn btn-lg btn-primary btn-block" name="submit" value="submit" type="submit">Sign in</button>
    <p class="mt-5 mb-3 text-muted">&copy; 2019</p>
</form>