<?php
class Assetter {

    private $jsCollection;
    private $cssCollection;

    public function __construct() {
        $this->jsCollection = array();
        $this->cssCollection = array();
    }    

    public function addJs($js) {
        if(!empty($js)) {
            $this->jsCollection[] = '<script src="' . $js . '"></script>';            
        }
    }

    public function addCss($css) {
        if(!empty($css)) {
            $this->cssCollection[] = '<link rel="stylesheet" type="text/css" href="' . $css . '" />';
        }
    }

    public function outputJs() {
        if(!empty($this->jsCollection))
        {
            echo implode("\n", $this->jsCollection);
        }
    }

    public function outputCss() {
        if(!empty($this->cssCollection))
        {
            echo implode("\n", $this->cssCollection);
        }
    }
}