<?php
class MessageModel extends Model {

    public function index() {

    }    

    public function getAll() {
        $this->query('SELECT * FROM messages ORDER BY last_update DESC');
        $rows = $this->resultset();
        return $rows;
    }

    public function create() {
        $res = array(
            'status' => '',
            'error' => ''      
        );
        
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);  
        //$res['data'] = $post;
        // invalid requests      
        if(empty($post['guest_name']) || empty($post['body'])) {
            
            $res['status'] = 'error';
            $res['error'] = 'Invalid Request.';
        } else if(strlen($post['guest_name']) > 50) {
            $res['status'] = 'error';
            $res['error'] = 'Name is too long!';
        }
        else if(strlen($post['body']) > 350) {
            $res['status'] = 'error';
            $res['error'] = 'Message is too long!';
        }
        else {
            // insert to db
            $this->query('INSERT INTO messages (guest_name, body) VALUES (:guest_name, :body)');
            $this->bind(':guest_name', $post['guest_name']);
            $this->bind(':body', $post['body']);            
            $this->execute();

            if($this->lastInsertId()) {
                $res['status'] = 'success';
            } else {
                $res['status'] = 'error';
                $res['error'] = 'Insert new post failed!';
            }
        }

        return $res;
    }

    public function update() {
        $res = array(
            'status' => '',
            'error' => ''      
        );
        
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);  
        //$res['data'] = $post;
        // invalid requests      
        if(empty($post['id']) || intval($post['id']) < 1 || empty($post['guest_name']) || empty($post['body'])) {            
            
            $res['status'] = 'error';
            $res['error'] = 'Invalid Request.';
        }  else if(strlen($post['guest_name']) > 50) {
            $res['status'] = 'error';
            $res['error'] = 'Name is too long!';
        }
        else if(strlen($post['body']) > 350) {
            $res['status'] = 'error';
            $res['error'] = 'Message is too long!';
        }
        else {
            // insert to db
            $this->query('UPDATE messages SET guest_name=:guest_name, body=:body WHERE id=:id');
            $this->bind(':guest_name', $post['guest_name']);
            $this->bind(':body', $post['body']);            
            $this->bind(':id', intval($post['id'])); 
            $this->execute();

            if($this->rowCount() > 0) {
                $res['status'] = 'success';
            } else {
                $res['status'] = 'error';
                $res['error'] = 'Edit post failed!';
            }
        }

        return $res;
    }

    public function delete() {
        $res = array(
            'status' => '',
            'error' => ''      
        );
        
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);  
        //$res['data'] = $post;
        // invalid requests      
        if(empty($post['id']) || intval($post['id']) < 1) {            
            
            $res['status'] = 'error';
            $res['error'] = 'Invalid Request.';
        } else {
            // insert to db
            $this->query('DELETE FROM messages WHERE id=:id');            
            $this->bind(':id', intval($post['id'])); 
            $this->execute();

            if($this->rowCount() > 0) {
                $res['status'] = 'success';
            } else {
                $res['status'] = 'error';
                $res['error'] = 'Delete post failed!';
            }
        }

        return $res;
    }
}